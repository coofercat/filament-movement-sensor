#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <digitalWriteFast.h>
#include <EEPROM.h>

#define SENSOR_FIRMWARE_VERSION "1.0"
#define EEPROM_DATA_VERSION 2

// 34.80 = 99.91mm, 99.94mm

#define DEFAULT_ENCODER_TICKS_PER_MM 34.78
#define DEFAULT_STEPPER_TICKS_PER_MM 522.08

#define FILAMENT_RUNOUT_INPUT_PIN 5
#define FILAMENT_RUNOUT_OUTPUT_PIN 6

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 32 // OLED display height, in pixels
#define INFO_WIDTH 10
#define GRAPH_WIDTH (SCREEN_WIDTH - INFO_WIDTH - 4)
#define GRAPH_MAX_READING (GRAPH_WIDTH / 2)
#define GRAPH_ORIGIN (GRAPH_WIDTH / 2)

#define FILAMENT_WIDTH (INFO_WIDTH - 2)
#define FILAMENT_HEIGHT 15

#define STEPPER_PULSE_PIN 2
#define STEPPER_DIR_PIN 3

#define ENCODER_PIN_A 9
#define ENCODER_PIN_B 10

#define MAX_FILAMENT_DIFF_PERIODS 10

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
#define OLED_RESET     -1 // Reset pin # (or -1 if sharing Arduino reset pin)
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

volatile bool _LeftEncoderASet;
volatile bool _LeftEncoderBSet;
volatile bool _LeftEncoderAPrev;
volatile bool _LeftEncoderBPrev;

struct CurrentPositionsStruct {
    unsigned long filamentForward;
    unsigned long filamentBackward;
    unsigned long stepperForward;
    unsigned long stepperBackward;
};

// If this struct changes, rev the EEPROM_DATA_VERSION too
struct SensorConfig {
    float StepperStepsPerMM;
    float FilamentStepsPerMM;
    unsigned int DisplayRefreshMS;
    byte DisplayMaxMMs;
    byte RunoutSwitch;
    byte SoftwareRunoutPercentage;
    byte SoftwareRunoutPeriods;
};

SensorConfig sensor_config;

struct EepromData {
    SensorConfig sensor_config;
    byte DataVersion;
    byte checksum;
};

volatile CurrentPositionsStruct currentpositions = { 0, 0, 0, 0 };

unsigned char heart_state = 0;

#define GCODE_BUFFER_SIZE 100

char gcode_read_buffer[GCODE_BUFFER_SIZE];
unsigned long gcode_last_n;
static char *gcode_strchr_pointer;
unsigned long gcode_N, Stopped_gcode_LastN = 0;
unsigned char gcode_in_error = 0;

char *gcode_cmd;
char gcode_out[100];

struct GraphPoint {
    byte filament_forward;
    byte stepper_forward;
    byte filament_backward;
    byte stepper_backward;
};

GraphPoint graph_points[SCREEN_HEIGHT];

// Keep track of time of screen updates so we can do it every "n" milliseconds
unsigned long last_screen_update_time = 0;

struct FilamentStateStruct {
    byte hardwareState;
    byte softwareState;
    byte runoutTriggered;
    unsigned int diffs[MAX_FILAMENT_DIFF_PERIODS];
    byte diff_index;
};

FilamentStateStruct filament_state = {
    1,
    1,
    0,
    {0,0,0,0,0,0,0,0,0,0},
    0,
};

// Pin change interrupt ISR for change on any of D8-13. Used for both encoder pins.
ISR (PCINT0_vect) {  // handle pin change interrupt for D8 to D13 here
  // Test transition;
  _LeftEncoderBSet = digitalReadFast(ENCODER_PIN_B);
  _LeftEncoderASet = digitalReadFast(ENCODER_PIN_A);
  
  if(_LeftEncoderAPrev && _LeftEncoderBPrev){
    if(!_LeftEncoderASet && _LeftEncoderBSet) {
      currentpositions.filamentForward++;
    } else if(_LeftEncoderASet && !_LeftEncoderBSet) {
      currentpositions.filamentBackward++;
    }
  } else if(!_LeftEncoderAPrev && _LeftEncoderBPrev){
    if(!_LeftEncoderASet && !_LeftEncoderBSet) {
      currentpositions.filamentForward++;
    } else if(_LeftEncoderASet && _LeftEncoderBSet) {
      currentpositions.filamentBackward++;
    }
  }else if(!_LeftEncoderAPrev && !_LeftEncoderBPrev){
    if(_LeftEncoderASet && !_LeftEncoderBSet) {
      currentpositions.filamentForward++;
    } else if(!_LeftEncoderASet && _LeftEncoderBSet) {
      currentpositions.filamentBackward++;
    }
  }else if(_LeftEncoderAPrev && !_LeftEncoderBPrev){
    if(_LeftEncoderASet && _LeftEncoderBSet) {
      currentpositions.filamentForward++;
    } else if(!_LeftEncoderASet && !_LeftEncoderBSet) {
      currentpositions.filamentBackward++;
    }
  }
  _LeftEncoderAPrev = _LeftEncoderASet;
  _LeftEncoderBPrev = _LeftEncoderBSet;
}

// Another ISR, this one is called every time we see a stepper pulse
void HandleStepperPulse(void) {
  if(digitalReadFast(STEPPER_DIR_PIN)) {
    currentpositions.stepperBackward++;
  } else {
    currentpositions.stepperForward++;
  }
}

void default_settings(void) {
    sensor_config = {
        DEFAULT_ENCODER_TICKS_PER_MM,
        DEFAULT_STEPPER_TICKS_PER_MM,
        250,
        10,
        0,
        0,
        3
    };
}

void write_eeprom(void) {
  EepromData data = {
      sensor_config,
      EEPROM_DATA_VERSION,
      0
  };

  // Calculate checksum
  int i;
  char *ptr = (char *)&data;
  for(i = 0; i < sizeof(data) - 1; i++) {
      data.checksum = data.checksum ^ (byte)*ptr++;
  }

  EEPROM.put(0, data);
}

int read_eeprom(void) {
    EepromData data;

    EEPROM.get(0, data);

    // Verify version
    if(data.DataVersion != EEPROM_DATA_VERSION) {
        return(0);
    }

    // Verify checksum
    int i;
    char *ptr = (char *)&data;
    byte checksum = 0;
    for(i = 0; i < sizeof(data) - 1; i++) {
        checksum = checksum ^ (byte)*ptr++;
    }

    if(checksum != data.checksum) {
        return(0);
    }

    // Keep the newly loaded config
    sensor_config = data.sensor_config;

    return(1);
}

void draw_heartbeat(unsigned char state) {
  // should draw a heart bitmap here, either large or small
  display.fillCircle(SCREEN_WIDTH - (INFO_WIDTH / 2), (INFO_WIDTH / 2), (INFO_WIDTH / 2) - 1, SSD1306_BLACK);

  if(state) {
    display.fillCircle(SCREEN_WIDTH - (INFO_WIDTH / 2), (INFO_WIDTH / 2), (INFO_WIDTH / 2) - 1, SSD1306_WHITE);
  } else {
    display.drawCircle(SCREEN_WIDTH - (INFO_WIDTH / 2), (INFO_WIDTH / 2), (INFO_WIDTH / 2) - 1, SSD1306_WHITE);
  }
}


#define FILAMENT_X1 (SCREEN_WIDTH - INFO_WIDTH)
#define FILAMENT_Y1 (SCREEN_HEIGHT - FILAMENT_HEIGHT - 1)
void draw_filament(unsigned char state) {
  display.fillRoundRect(FILAMENT_X1, FILAMENT_Y1, FILAMENT_WIDTH, FILAMENT_HEIGHT, 1, SSD1306_BLACK);
  if(state) {
    display.fillRoundRect(FILAMENT_X1, FILAMENT_Y1, FILAMENT_WIDTH, FILAMENT_HEIGHT, 1, SSD1306_WHITE);
  } else {
    display.drawRoundRect(FILAMENT_X1, FILAMENT_Y1, FILAMENT_WIDTH, FILAMENT_HEIGHT, 1, SSD1306_WHITE);
  }
}

int graph_points_index = SCREEN_HEIGHT;
void graph_point(byte filament_forward, byte stepper_forward, byte filament_backward, byte stepper_backward) {
  int i;

  if(filament_forward > GRAPH_MAX_READING) {
      filament_forward = GRAPH_MAX_READING;
  }
  if(stepper_forward > GRAPH_MAX_READING) {
      stepper_forward = GRAPH_MAX_READING;
  }
  if(filament_backward > GRAPH_MAX_READING) {
      filament_backward = GRAPH_MAX_READING;
  }
  if(stepper_backward > GRAPH_MAX_READING) {
      stepper_backward = GRAPH_MAX_READING;
  }

  graph_points[graph_points_index] = { filament_forward, stepper_forward, filament_backward, stepper_backward };

  unsigned int index = graph_points_index;
  for(i = 0; i<SCREEN_HEIGHT; i++) {
    display.drawLine(0, i, GRAPH_WIDTH, i, SSD1306_BLACK);
    display.drawLine(GRAPH_ORIGIN - graph_points[index].stepper_forward, i, GRAPH_ORIGIN - graph_points[index].filament_forward, i, SSD1306_WHITE);
    display.drawLine(GRAPH_ORIGIN + graph_points[index].stepper_backward, i, GRAPH_ORIGIN + graph_points[index].filament_backward, i, SSD1306_WHITE);

    index++;
    if(index >= SCREEN_HEIGHT) {
        index = 0;
    }
  }

  graph_points_index--;
  if(graph_points_index < 0) {
      graph_points_index = SCREEN_HEIGHT - 1;
  }
}

void pciSetup(byte pin)
{
    *digitalPinToPCMSK(pin) |= bit (digitalPinToPCMSKbit(pin));  // enable pin
    PCIFR  |= bit (digitalPinToPCICRbit(pin)); // clear any outstanding interrupt
    PCICR  |= bit (digitalPinToPCICRbit(pin)); // enable interrupt for the group
}

void setup() {
  int i;
  Serial.begin(115200);
  Serial.print(F("echo:; Filament Movement Sensor starting up...\n"));
  gcode_last_n = 0;
  gcode_N = 0;

  default_settings();
  if(read_eeprom()) {
      Serial.print(F("echo:; Configuration read from EEPROM\n"));
  } else {
      Serial.print(F("echo:; EEPROM read failure - using default settings\n"));
  }

  pinMode(FILAMENT_RUNOUT_INPUT_PIN, INPUT);
  digitalWrite(FILAMENT_RUNOUT_INPUT_PIN, HIGH);  // turn on pull up
  pinMode(FILAMENT_RUNOUT_OUTPUT_PIN, OUTPUT);
  digitalWrite(FILAMENT_RUNOUT_OUTPUT_PIN, LOW);

  // Shaft encoder
  pinMode(ENCODER_PIN_A, INPUT_PULLUP); // set pinA as an input, pulled HIGH to the logic voltage (5V or 3.3V for most cases)
  pinMode(ENCODER_PIN_B, INPUT_PULLUP); // set pinB as an input, pulled HIGH to the logic voltage (5V or 3.3V for most cases)
  
  // Stepper watcher
  pinMode(STEPPER_PULSE_PIN, INPUT_PULLUP);
  pinMode(STEPPER_DIR_PIN, INPUT_PULLUP);

  attachInterrupt(digitalPinToInterrupt(STEPPER_PULSE_PIN), HandleStepperPulse, RISING);

  pciSetup(ENCODER_PIN_A);
  pciSetup(ENCODER_PIN_B);

  // SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
  if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { // Address 0x3C for 128x32
    Serial.println(F("SSD1306 allocation failed"));
    for(;;); // Don't proceed, loop forever
  }
  display.display();
  display.clearDisplay();

  display.drawLine(GRAPH_WIDTH / 2, 0, GRAPH_WIDTH / 2, SCREEN_HEIGHT, SSD1306_WHITE);
  display.display();

  // Initialise the graph points data structure
  for(i=0; i<SCREEN_HEIGHT; i++) {
    graph_points[i] = { 0, 0, 0, 0 };
  }
}

int diff_unsigned_with_overflow(unsigned int a, unsigned int b) {
  unsigned int distance;
 
  distance = b - a;
 
  if(b >= a) {
    return (int)distance;
  } else {
    // check for overflow
    if((a + distance) < a) {
      // overflow
      return int(distance);
    }
    // Not an overflow
    return (int)(0 - distance);
  }  
}

double filament_to_mm(unsigned long ticks) {
  return (double)ticks / sensor_config.FilamentStepsPerMM;
}

double stepper_to_mm(unsigned long ticks) {
  return (double)ticks / sensor_config.StepperStepsPerMM;
}

unsigned int last_screen_filament_forward = 0;
unsigned int last_screen_filament_backward = 0;
unsigned int last_serial_filament_forward = 0;
unsigned int last_serial_filament_backward = 0;

unsigned int last_screen_stepper_forward = 0;
unsigned int last_screen_stepper_backward = 0;
unsigned int last_serial_stepper_forward = 0;
unsigned int last_serial_stepper_backward = 0;

unsigned char screen_update_counter = 0;
unsigned char serial_update_counter = 0;

void screen_update() {
  int filament_forward_diff = 0;
  int filament_backward_diff = 0;
  int stepper_forward_diff = 0;
  int stepper_backward_diff = 0;
  
  if(currentpositions.filamentForward != last_screen_filament_forward) {
    filament_forward_diff = diff_unsigned_with_overflow(last_screen_filament_forward, currentpositions.filamentForward);
    last_screen_filament_forward = currentpositions.filamentForward;
  }
  if(currentpositions.filamentBackward != last_screen_filament_backward) {
    filament_backward_diff = diff_unsigned_with_overflow(last_screen_filament_backward, currentpositions.filamentBackward);
    last_screen_filament_backward = currentpositions.filamentBackward;
  }

  if(currentpositions.stepperForward != last_screen_stepper_forward) {
    stepper_forward_diff = diff_unsigned_with_overflow(last_screen_stepper_forward, currentpositions.stepperForward);
    last_screen_stepper_forward = currentpositions.stepperForward;
  }
  if(currentpositions.stepperBackward != last_screen_stepper_backward) {
    stepper_backward_diff = diff_unsigned_with_overflow(last_screen_stepper_backward, currentpositions.stepperBackward);
    last_screen_stepper_backward = currentpositions.stepperBackward;
  }

  float time_diff = (millis() - last_screen_update_time) / 1000;

  float multiplier = GRAPH_MAX_READING / sensor_config.DisplayMaxMMs;

  filament_forward_diff = (int)( ((float)filament_to_mm(filament_forward_diff) / time_diff) * multiplier);
  filament_backward_diff = (int)( ((float)filament_to_mm(filament_backward_diff) / time_diff) * multiplier);
  stepper_forward_diff = (int)( ((float)stepper_to_mm(stepper_forward_diff) / time_diff) * multiplier);
  stepper_backward_diff = (int)( ((float)stepper_to_mm(stepper_backward_diff) / time_diff) * multiplier);

  graph_point((byte)filament_forward_diff, (byte)stepper_forward_diff, (byte)filament_backward_diff, (byte)stepper_backward_diff);

  draw_filament(digitalRead(FILAMENT_RUNOUT_INPUT_PIN));
  draw_heartbeat(heart_state);
  heart_state = !heart_state;

  display.display();
}

// Get the hardware filament switch state, honouring the NO/NC config
byte get_hardware_filament_state(void) {
  byte hardware_pin = digitalRead(FILAMENT_RUNOUT_INPUT_PIN);

  switch(sensor_config.RunoutSwitch) {
      case 0:
           // Not used
           return 1;
           break;
       case 1:
           // Normally Open (ie. 1 = no filament)
           if(hardware_pin == 1) {
               return 0;
           }
           return 1;
           break;
       case 2:
           // Normally Closed (ie. 1 = filament present)
           if(hardware_pin == 1) {
               return 1;
           }
           return 0;
           break;
  }
  // All other cases = "not used"
  return 1;
}

byte get_filament_state(void) {
    byte hardware_state = get_hardware_filament_state();

    if(hardware_state == 0) {
        if(filament_state.hardwareState == 1) {
	    // filament runout has been triggered
            filament_state.hardwareState = 0;
            return 0;
        } else {
            return 0;
        }
    } else {
        filament_state.hardwareState = 1;
    }

    // By here, hardware doesn't say we're out of filament
    // Now work out software state
    byte software_state = 1;

    float slip_percentage = 0;
    int i;
    byte index = filament_state.diff_index;
    for(i = 0; i < sensor_config.SoftwareRunoutPeriods; i++) {
        slip_percentage = slip_percentage + filament_state.diffs[index++];
        if(index > sensor_config.SoftwareRunoutPeriods) {
            index = 0;
        }
    }

    // Now calculate the outcome...
    if(software_state == 0) {
        if(filament_state.softwareState == 1) {
            filament_state.softwareState = 0;
            return 0;
        } else {
            return 0;
        }
    } else {
        filament_state.softwareState = 1;
    }

    return 1;
}



void serial_write_position() {
  // output in gcode-esque style
  Serial.print("ok EF:");
  Serial.print(stepper_to_mm(currentpositions.stepperForward), 2);
  Serial.print(" EB:");
  Serial.print(stepper_to_mm(currentpositions.stepperBackward), 2);

  Serial.print(" FF:");
  Serial.print(filament_to_mm(currentpositions.filamentForward), 2);
  Serial.print(" FB:");
  Serial.print(filament_to_mm(currentpositions.filamentBackward), 2);
  
  Serial.println("");

}

int gcode_index = 0;

void loop() {
  if(millis() - last_screen_update_time > sensor_config.DisplayRefreshMS) {
    screen_update();
    last_screen_update_time = millis();
  }

  if(Serial.available() > 0) {
    int c = Serial.read();
    if(c == '\r') {
      return;
    }
    if(c == '\n') {
      gcode_read_buffer[gcode_index] = '\0';
      process_gcode_command();
      gcode_index = 0;
    } else if(gcode_index < GCODE_BUFFER_SIZE) {
      gcode_read_buffer[gcode_index++] = (char)c;
    }
  } else {
      delay(1);
  }
}

void set_setting(char setting, char *buffer) {
    int value = abs(atoi(buffer));
    switch(setting) {
        case 'E':
		sensor_config.StepperStepsPerMM = abs(atof(buffer));
		break;
	case 'F':
		sensor_config.FilamentStepsPerMM = abs(atof(buffer));
		break;
	case 'R':
		if(value <= 2) {
		    sensor_config.RunoutSwitch = value;
		}
		break;
	case 'S':
		if(value <= 100) {
		    sensor_config.SoftwareRunoutPercentage = atoi(buffer);
		}
		break;
	case 'P':
		if(value <= MAX_FILAMENT_DIFF_PERIODS) {
		    sensor_config.SoftwareRunoutPeriods = value;
		}
		break;
	case 'D':
		if(value <= 1000) {
		    sensor_config.DisplayRefreshMS = value;
		}
		break;
	case 'M':
		if(value <= 100) {
		     sensor_config.DisplayMaxMMs = value;
		}
		break;
    }
}

void process_m92_settings(void) {
    int i;
    int number_start = -1;
    char setting;
    char buffer[10];
    byte buffer_index;

    for(i=4; i <= strlen(gcode_cmd); i++) {
        if(number_start > 0) {
            if(! ((gcode_cmd[i] >= '0') && (gcode_cmd[i] <= '9')) || (gcode_cmd[i] == '.')) {
		// is not a number or .
		set_setting(setting, &gcode_cmd[number_start]);
		number_start = -1;
            }
        }
        if((gcode_cmd[i] >= 'A') && (gcode_cmd[i] <= 'Z')) {
            setting = gcode_cmd[i];
            number_start = i+1;
            buffer_index = 0;
        }
    }
}

void process_gcode_command(void) {
  int i;
  int number_start;

  gcode_cmd = gcode_read_buffer;
  gcode_N = 0;

  if(strchr(gcode_read_buffer, 'N') != NULL)
  {
    gcode_strchr_pointer = strchr(gcode_read_buffer, 'N');
    gcode_cmd = strchr(gcode_strchr_pointer, ' ');
    gcode_cmd++;
    gcode_N = (strtol(&gcode_read_buffer[gcode_strchr_pointer - gcode_read_buffer + 1], NULL, 10));
    if(gcode_N != gcode_last_n + 1 && (strstr_P(gcode_read_buffer, PSTR("M110")) == NULL) ) {
      sprintf(gcode_out, "Error:Line Number is not Last Line Number+1, Last Line: %d\n", gcode_last_n);
      Serial.write(gcode_out);
      sprintf(gcode_out, "Resend %d\n", gcode_last_n + 1);
      Serial.write(gcode_out);
      return;
    }

    if(strchr(gcode_read_buffer, '*') != NULL)
    {
       byte checksum = 0;
       byte count = 0;
       while(gcode_read_buffer[count] != '*') checksum = checksum^gcode_read_buffer[count++];
       gcode_strchr_pointer = strchr(gcode_read_buffer, '*');
       
       if( (int)(strtod(&gcode_read_buffer[gcode_strchr_pointer - gcode_read_buffer + 1], NULL)) != checksum) {
         sprintf(gcode_out, "Error:checksum mismatch, Last Line: %d\n", gcode_last_n);
         Serial.write(gcode_out);
         sprintf(gcode_out, "Resend %d\n", gcode_last_n + 1);
         Serial.write(gcode_out);
         return;
       }
       //if no errors, continue parsing
    }
    gcode_last_n = gcode_N;
  }
      
  // to be here, we've got a line of buffer in
  if(strcmp(gcode_cmd, "M105") == 0) { // get temperatures
    // We dummy these to maintain compatability with printers, but we don't use
    // temperatures for anything
    Serial.print(F("ok T:150.1/200.0 B:64.4/70.0 T0:150.1/200.0 @:0 B@:0\n"));
    return;
  } else if(strcmp(gcode_cmd,"M114") == 0) { // "get position"
    // return the position of the filament and sensor
    serial_write_position();
    return;
  } else if(strncmp(gcode_cmd,"M106", 4) == 0) { // fan on (optionally with percentage)
    // turn on the 'fan'
    digitalWrite(13, HIGH);
  } else if(strcmp(gcode_cmd, "M107") == 0) { // fan off
    // Turn the 'fan' off
    digitalWrite(13, LOW);
  } else if(strcmp(gcode_cmd, "M110") == 0) { // reset line numbering
    // Reset our "N" counter
    gcode_last_n = 0;
    gcode_N = 0;
  } else if(strcmp(gcode_cmd, "M503") == 0) { // show settings
    Serial.println(F("echo:  G21    ; Units in mm (mm)"));
    Serial.println(F("echo:; Steps per unit and other settings:"));
    Serial.print(F("echo: M92 E:"));
    Serial.print(sensor_config.StepperStepsPerMM);
    Serial.print(F(" F:"));
    Serial.print(sensor_config.FilamentStepsPerMM);
    Serial.print(F(" R:"));
    Serial.print(sensor_config.RunoutSwitch);
    Serial.print(F(" S:"));
    Serial.print(sensor_config.SoftwareRunoutPercentage);
    Serial.print(F(" P:"));
    Serial.print(sensor_config.SoftwareRunoutPeriods);
    Serial.print(F(" D:"));
    Serial.print(sensor_config.DisplayRefreshMS);
    Serial.print(F(" M:"));
    Serial.print(sensor_config.DisplayMaxMMs);
    Serial.println();
  } else if(strncmp(gcode_cmd,"M92", 3) == 0) { // set configuration (in memory only!)
    process_m92_settings();
  } else if(strcmp(gcode_cmd, "G28") == 0) { // return to origin
    // We'll use this as "reset the counters"
    currentpositions.stepperForward = 0;
    currentpositions.stepperBackward = 0;
    currentpositions.filamentForward = 0;
    currentpositions.filamentBackward = 0;
  } else if(strcmp(gcode_cmd, "M501") == 0) { // load EEPROM
    if(read_eeprom() == 1) {
        Serial.println(F("echo:; EEPROM settings read sucessfully"));
    } else {
        Serial.println(F("echo:; EEPROM settings read failed"));
    }
  } else if(strcmp(gcode_cmd, "M500") == 0) { // write EEPROM
    write_eeprom();
  } else if(strcmp(gcode_cmd, "M502") == 0) { // default settings
    default_settings();
  } else if(strcmp(gcode_cmd, "RESET") == 0) { // fake gcode, used for debugging
    gcode_last_n = 0;
    gcode_N = 0;
    digitalWrite(13, LOW);
  } else {
    Serial.println("ok");
    return;
  }
  Serial.println("ok");
}
