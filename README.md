# filament-movement-sensor

Hardware and software to monitor the movement of filament in a 3D printer.

This project requires some 3D printing, an Arduino, a shaft encoder and
some other bits and bats.

# TODO

Things to do on this project:

* Write some proper documentation on how to make the sensor, wire up the
  electronics, connect to the printer etc.

* Tidy up the code a bit

* Make the screen graph more useful. It's a temporary solution at the moment
  and should show differences in filament movement

* Implement EEPROM config reading/writing and the GCODE to set the calibration
  and to save it in the EEPROM.

* Add features to be able to trigger a "filament run out" event if 'requested'
  differs from 'actual' by too much for too long. Add in GCODE to enable/disable
  the feature.

* Test with 1.75mm filament
