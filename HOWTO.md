# How to Make the Filament Movement Sensor

_Note: This page would benefit from some pictures!_

There are several parts to this problem:

  1. Collect the required parts
  1. Print the sensor plastic parts
  1. Make the electronics
  1. Assemble the plastic part
  1. Mount the plastic part to the printer
  1. Mount the electronics, or else hide them somewhere (optional)
  1. Connect to Octoprint (optional, but highly recommended)

## Collect the Required Parts

For this project, you will need:

- A 3D Printer ;-)
- small elastic bands (eg. dental elastic bands like these: https://smile.amazon.co.uk/100pcs-Dental-Orthodontic-Elastics-Braces/dp/B07ZF5T6Q1 - 3/16" seem to work well)
- A shaft encoder (eg. a cheap Chinese 600ppr one like this: https://smile.amazon.co.uk/gp/product/B07W54CH5D. It's possible different encoders will also work, so long
as they're AB or two phase and work at 5V. If they're a different size then you
may need to work on the plastic parts to make it all fit though.
- An Arduino (any Mega328 type will do, eg Duemilanove or Nano)
- An optional I2C display. The only type tested so far is an OLED SSD1306
  module like this one: https://smile.amazon.co.uk/gp/product/B07BDFXFRK
- Some small screws and some M3 machine screws (_what sizes!?_)

You'll probably also need some random veroboard, connectors, wire, a
soldering iron and some spare time.

## Print the Sensor Plastic Parts

The large parts can be printed in draft/quick speed, but I'd recommend
using "fine" or high quality for the wheels and wheel holder. The sensor
benefits when the wheels are round and smooth!

## Make the electronics

_To follow_

Shaft encoder goes to pins 9 and 10. The stepper connection goes to pins 2
and 3 and the (optional) display to pins 4 and 5. Pins are pretty much
fixed due to the hardware interrupts on the chip.

The sensor benefits greatly from a connection to the stepper driver for
the extruder. Most printer electronics boards do not break this connection
out for easy interfacing though. You need to find a connection to the
"step" (or "pulse") and "direction" pins on the stepper driver. A ground
connection is also beneficial.

The sensor has a hardware "filament runout" input and output. These are
currently unused, but will allow for pausing a print if
filament runs out or has stopped moving (eg. when the nozzle is blocked).
You can connect the output to most printer electronics boards, or to the
GPIO if you're using Octoprint on a Raspberry Pi.

## Assemble the Plastic Parts

_To follow_

The wheels both need one, or ideally two small elastic bands around them.
Dental elastic bands seem to be easy to obtain and work really well.
Try to get the bands to be flat (not twisted) and next to each other
on the wheel.

## Mount the Plastic Part to the Printer

This varies by printer type, and if the printer's been modified in any way.
The plastic part has some (hopefully) useful general purpose screw holes
so mounting to a printer should be reasonably straight forward, so long as
there's some space available.

The critical part of this is that the filament needs to be as straight as
possible from the reel, through the sensor and into the extruder. You may
find a gap of 20-30mm between the sensor and extruder useful to aid
threading the filament.

## Mount the Electronics

Ideally, put the electronics in a box and stick it somewhere out of the
way on the printer. This step obviously depends on how you made the
electronics and what printer you have.

_Todo: Design and include a suitable box for electronics, if made as
described_

## Connect to Octoprint (optional)

The real benefit of this sensor comes when you can see (in real time) the
movement of filament during a print. The little (optional) display is
handy, but a proper graph on a proper screen is far superior. You can
connect the sensor to Octoprint via a suitable USB cable plugged into the
Arduino. This can provide power and a data connection so that Octoprint
can read the sensor's state.

See the Octoprint Plugin that makes all this possible:
https://gitlab.com/coofercat/octoprint-filamentmovement
